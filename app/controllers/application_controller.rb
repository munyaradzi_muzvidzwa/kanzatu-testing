require 'resolv-replace'
class ApplicationController < ActionController::Base
  #config.assets.paths << Rails.root.join("app/assets/images").to_s

	#@@conn = Faraday.new(:url => 'https://digitalbanking.stewardbank.co.zw/squareworldmicroservice/api/', :ssl => {:verify => false}) do |faraday|
  @@conn = Faraday.new(:url => 'https://192.168.1.34:9034/api/', :ssl => {:verify => false}) do |faraday|
		faraday.request  :url_encoded             
		faraday.response :logger                  
		faraday.adapter  :httpclient  
	end
end
