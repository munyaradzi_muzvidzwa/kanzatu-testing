require 'faraday'
require 'base64'
require 'uri'
require 'resolv-replace'
require 'google/cloud/storage'
class PaymentController < ApplicationController
  skip_before_action :verify_authenticity_token
  def new

  end

  def index
    begin
      storage = Google::Cloud::Storage.new project: "squarev2-87787", keyfile: '/home/ubuntu/ruby_projects/kanzatu/SquareV2-a0f3ecfc1a54.json'
      storage.buckets.each do |bucket|
        puts bucket.name
      end
    rescue Exception => e
      puts e.message
    end

    @campaign_target = 0

    puts params[:first_param]
    if params[:first_param] === "sbconcert"
      @mobile = "263773390349"
    elsif params[:first_param] === "cholera"
      @mobile = "263771222727"
    else
      @mobile = params[:first_param]
      @mobile = @mobile[-9..-1] || @mobile
      @mobile = "263"+@mobile
    end
    puts "The mobile is "+@mobile
    @image_url = "http://storage.googleapis.com/squarev2-87787.appspot.com/kanzatu/#{@mobile}.jpg"
    @campaign_address = "https://digitalbanking.stewardbank.co.zw/kanzatu/#{@mobile}"
    @campaign_description = "Coming together is the beginning. Keeping together is progress. Working together is success. Lets all join together to make this campaign a success."
    @campaign_name = "Kanzatu-nzatu Crowdfunding"

    if !params[:view_campaign].nil?
      showSingleCampaign(params[:view_campaign])
    elsif !params[:paynow_amount].nil?
      triggerPaynow(params[:bill_account], @mobile, params[:paynow_amount])
    elsif !params[:square_confirm].nil?
      confirmSquare(params[:bill_account], params[:square_account], params[:square_amount], params[:square_mobile], params[:square_otp], @mobile, params[:campaign], params[:campaign_description])
    elsif !params[:donate_square].nil?
      triggerSquare(params[:bill_account], params[:square_account], params[:square_amount], params[:square_mobile], params[:square_pin], @mobile, params[:campaign], params[:campaign_description])
    elsif !params[:payment_option].nil?
      if params[:payment_option] === "ECOCASH"
        donateWithEcocash(params[:bill_account], params[:phone], params[:campaign], params[:campaign_description])
      elsif params[:payment_option] === "PAYNOW"
        donateWithPaynow(params[:bill_account], params[:phone], params[:campaign], params[:campaign_description])
      elsif params[:payment_option] === "SQUARE"
        donateWithSquare(params[:bill_account], params[:phone], params[:campaign], params[:campaign_description])
      elsif params[:payment_option] === "ZIPIT"
        @campaign_description = params[:campaign_description]
        @campaign_name = params[:campaign]
        begin
          render 'zipit'
        rescue Exception => e
          puts e.message
        end
      end 
    elsif !params[:verify_ecocash].nil?
      showSingleCampaign(params[:bill_account])
    elsif !params[:ecocash_mobile].nil?
      triggerEcocash(params[:bill_account], params[:phone], params[:ecocash_mobile], params[:ecocash_amount], @mobile, params[:campaign], params[:campaign_description])
    else
      if @mobile != "263771222727"
        showAllCampaigns()
      else
        if is_mobile?
          render 'cholera_details'
        else
          render 'cholera_details'
        end
      end
    end
  end

  def donateWithEcocash(bill_account, phone, campaign, campaign_description)
    @campaign = campaign
    @campaign_description = campaign_description
    puts @campaign_description
    render 'donate_ecocash'
  end

  def donateWithPaynow(bill_account, phone, campaign, campaign_description)
    @campaign = campaign
    @campaign_description = campaign_description
    render 'donate_paynow'
  end

  def donateWithSquare(bill_account, phone, campaign, campaign_description)
    @campaign = campaign
    @campaign_description = campaign_description
    render 'donate_square'
  end

  def trigger_square(bill_account, account)
    begin
      puts params
      render 'square_otp'
    rescue
      e.message
    end
  end 

  def triggerSquare(bill_account, account, amount, square_mobile, square_pin, phone, campaign, campaign_description)
    square_mobile = square_mobile[-9..-1] || square_mobile
    square_mobile = "263"+square_mobile

    @image_url = "http://storage.googleapis.com/squarev2-87787.appspot.com/kanzatu/#{phone}.jpg"
    @campaign_address = "https://digitalbanking.stewardbank.co.zw/kanzatu/#{phone}"

    @campaign = campaign
    @campaign_description = campaign_description

    begin
      triggerSquareResponse = @@conn.post do |req|
        req.url 'gofundme/secured/campaign/authenticate'
        req.headers['Content-Type'] = 'application/json'
        req.body = '{ "channel": "MobileWorld", "biller": "KANZATU", "account":"'+account+'", "amount": '+amount+', "username":"'+square_mobile+'", "password":"'+square_pin+'", "billAccount":"'+bill_account+'" }'
        puts req.body
      end

      triggerSquareString = JSON.parse(triggerSquareResponse.body)
      puts triggerSquareString["message"]+triggerSquareString["responseBody"]
      case triggerSquareString["message"]
      when "FAILED"
        puts "First I am failed"
        render 'mismatch'
      when "SUCCESS"
        render 'square_otp'
      else
        render 'error_page'
      end
    rescue Exception => e
      puts e.message
      render 'error_page'
    end 
  end

  def confirmSquare(bill_account, square_account, amount, square_mobile, square_otp, mobile, campaign, campaign_description)
    square_mobile = square_mobile[-9..-1] || square_mobile
    square_mobile = "263"+square_mobile

    @image_url = "http://storage.googleapis.com/squarev2-87787.appspot.com/kanzatu/#{mobile}.jpg"
    @campaign_address = "https://digitalbanking.stewardbank.co.zw/kanzatu/#{mobile}"

    @campaign = campaign
    @campaign_description = campaign_description

    begin
      triggerSquareResponse = @@conn.post do |req|
        req.url 'gofundme/secured/campaign/activateauthenticate'
        req.headers['Content-Type'] = 'application/json'
        req.body = '{ "channel": "MobileWorld", "account":"'+square_account+'", "phone":"'+square_mobile+'", "otp": "'+square_otp+'" }'
        puts req.body
      end

      triggerSquareString = JSON.parse(triggerSquareResponse.body)
      puts triggerSquareString["responseBody"]
      case triggerSquareString["message"]
      when "FAILED"
        puts "First I am failed"
        render 'error_page'
      when "SUCCESS"
        @token = triggerSquareString["responseBody"]
        #amount_cents = ((amount.to_f)*100).to_s
        begin
          depositResponse = @@conn.post do |req|
            req.url 'gofundme/secured/campaign/deposit'
            req.headers['Content-Type'] = 'application/json'
            req.body = '{ "token": "'+@token+'", "phone": "'+square_mobile+'", "biller": "KANZATU", "amount": '+amount+', "payerName": "'+square_mobile+'", "billAccount": "'+bill_account+'" }'
            puts req.body
          end

          depositString = JSON.parse(depositResponse.body)
          puts depositString["message"]
          case depositString["message"]
          when "FAILED"
            puts "First I am failed"
            render 'error_page'
          when "SUCCESS"
            render 'success'
          else
            render 'error_page'
          end
        rescue Exception => e
          puts e.message
          render 'error_page'
        end
      else
        fail "Invalid response #{response.to_str} received."
      end
    rescue Exception => e
      puts e.message
      render 'error_page'
    end 
  end

  def triggerPaynow(bill_account, phone, paynow_amount)
    @image_url = "http://storage.googleapis.com/squarev2-87787.appspot.com/kanzatu/#{phone}.jpg"
    @campaign_address = "https://digitalbanking.stewardbank.co.zw/kanzatu/#{phone}"

    begin
      triggerPaynowResponse = @@conn.post do |req|
        req.url 'paynow/secured/paynow/kanzatu/initiatetransaction'
        req.headers['Content-Type'] = 'application/json'
        req.body = '{ "channel": "MobileWorld", "biller": "KANZATU","amount": '+paynow_amount+', "phoneNumber":"'+phone+'", "accountNo":"'+bill_account+'" }'
        puts req.body
      end

      triggerPaynowString = JSON.parse(triggerPaynowResponse.body)
      puts triggerPaynowString["responseBody"]
      case triggerPaynowString["message"]
      when "FAILED"
        puts "First I am failed"
        render 'error_page'
      when "SUCCESS"
        redirect_to URI.decode(triggerPaynowString["responseBody"])
      else
        fail "Invalid response #{response.to_str} received."
      end
    rescue Exception => e
      puts e.message
      render 'error_page'
    end 
  end

  def triggerEcocash(bill_account, phone, ecocash_mobile, ecocash_amount, mobile, campaign, campaign_description)
    puts "What i received "+ecocash_mobile+" "+ecocash_amount
    ecocash_mobile = ecocash_mobile[-9..-1] || ecocash_mobile
    ecocash_mobile = "263"+ecocash_mobile

    @image_url = "http://storage.googleapis.com/squarev2-87787.appspot.com/kanzatu/#{mobile}.jpg"
    @campaign_address = "https://digitalbanking.stewardbank.co.zw/kanzatu/#{mobile}"
    @campaign = campaign
    @campaign_description = campaign_description

    begin
      triggerEcocashResponse = @@conn.post do |req|
        req.url 'ecocashcontainer/secured/ecocash/pay'
        req.headers['Content-Type'] = 'application/json'
        req.body = '{ "channel": "MobileWorld", "phone": "'+ecocash_mobile+'", "biller": "KANZATU", "amount": "'+ecocash_amount+'", "billAccount": "'+bill_account+'" }'
        puts req.body
      end

      triggerEcocashString = JSON.parse(triggerEcocashResponse.body)
      puts triggerEcocashString["message"]
      case triggerEcocashString["message"]
      when "FAILED"
        puts "First I am failed"
        render 'error_page'
      when "SUCCESS"
        @ecocash_code = triggerEcocashString["responseBody"]
        render 'ecocash_confirm'
      else
        fail "Invalid response #{response.to_str} received."
      end
    rescue Exception => e
      puts e.message
      render 'error_page'
    end 
  end

  def showAllCampaigns
    @image_url = "http://storage.googleapis.com/squarev2-87787.appspot.com/kanzatu/#{@mobile}.jpg"
    @campaign_address = "https://digitalbanking.stewardbank.co.zw/kanzatu/#{@mobile}"

    begin
      myCampaignsResponse = @@conn.post do |req|
        req.url 'gofundme/secured/myCampaigns'
        req.headers['Content-Type'] = 'application/json'
        req.body = '{ "channel": "MobileWorld", "phone": "'+@mobile+'" }'
      end

      bodyString = JSON.parse(myCampaignsResponse.body)
      puts bodyString["message"]
      case bodyString["message"]
      when "FAILED"
        puts "First I am failed"
        render 'error_page'
      when "SUCCESS"
        puts "The size is "+bodyString["responseBody"].size.to_s
        if bodyString["responseBody"].size > 0
          begin
            @image = ""
            # @image = bodyString["responseBody"][0]["image"]
            # @image = "data:image/jpeg;base64,"+@image
            @campaign_name = bodyString["responseBody"][0]["campaignName"]
            @campaign_target = bodyString["responseBody"][0]["target"]
            @campaign_description = bodyString["responseBody"][0]["description"]
            @campaign_id = bodyString["responseBody"][0]["identifier"]
            @created_by = bodyString["responseBody"][0]["originalName"]
          rescue Exception => e
            puts e.message
          end
          render 'details'
        else
          render 'no_campaign'
        end
      else
        fail "Invalid response #{response.to_str} received."
      end
    rescue Exception => e
      puts e.message
      render 'error_page'
    end 
  end

  def showSingleCampaign(campaign_id)


    @total_contributions = "_"
    @total_contributors = "_"
    @device_is_mobile = false;
    puts "__________________________"
    begin
      @device_is_mobile = is_mobile?
      puts "Mobile status is "+@device_is_mobile.to_s
    rescue Exception => e
      puts e.message
    end 

    @image_url = "http://storage.googleapis.com/squarev2-87787.appspot.com/kanzatu/#{@mobile}.jpg"
    @campaign_address = "https://digitalbanking.stewardbank.co.zw/kanzatu/#{@mobile}"
    
    begin
      thisCampaignResponse = @@conn.post do |req|
        req.url 'gofundme/secured/myCampaigns'
        req.headers['Content-Type'] = 'application/json'
        req.body = '{ "channel": "MobileWorld", "phone": "'+@mobile+'" }'
        puts req.body
      end

      thisCampaignString = JSON.parse(thisCampaignResponse.body)
      puts thisCampaignString
      case thisCampaignString["message"]
      when "FAILED"
        puts "First I am failed"
        render 'error_page'
      when "SUCCESS"
        begin
          # @image = thisCampaignString["responseBody"][0]["image"]
          # @image = "data:image/jpeg;base64,"+@image
          @image = ""
          @img = thisCampaignString["responseBody"][0]["phone"]
          @campaign_name = thisCampaignString["responseBody"][0]["campaignName"]
          @campaign_target = thisCampaignString["responseBody"][0]["target"]
          @campaign_description = thisCampaignString["responseBody"][0]["description"]
          @account = thisCampaignString["responseBody"][0]["account"]
          @campaign_id = thisCampaignString["responseBody"][0]["identifier"]
          @created_by = thisCampaignString["responseBody"][0]["originalName"]

          @address = "https://digitalbanking.stewardbank.co.zw/kanzatu/#{@phone}"
        rescue Exception => e
          puts e.message
        end
      else
        fail "Invalid response #{response.to_str} received."
      end
    rescue Exception => e
      puts e.message
    end 
    begin
      contributionsResponse = @@conn.get do |req|
        req.url 'gofundme/secured/campaign/getTotalContribution/'+campaign_id
        req.headers['Content-Type'] = 'application/json'
      end
      contributionsString = JSON.parse(contributionsResponse.body)
      puts contributionsResponse.body
      case contributionsString["message"]
      when "FAILED"
        puts "Failed to get total of contributions"
      when "SUCCESS"
        @total_contributions = contributionsString["responseBody"]
      end
    rescue Exception => e
      puts e.message
    end 
    begin
      contributorsResponse = @@conn.get do |req|
        req.url 'gofundme/secured/campaign/getContributionHistory/'+campaign_id
        req.headers['Content-Type'] = 'application/json'
      end
      contributorsString = JSON.parse(contributorsResponse.body)
      puts contributorsString["message"]
      case contributorsString["message"]
      when "FAILED"
        puts "Failed to get total of contributors"
      when "SUCCESS"
        @total_contributors = contributorsString["responseBody"].size
      end
    rescue Exception => e
      puts e.message
    end 
    render 'single'
  end

  def is_mobile?
    request.user_agent =~ /Mobile|webOS|iPhone/
  end

end
