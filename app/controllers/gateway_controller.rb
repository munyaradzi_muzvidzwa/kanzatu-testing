require 'faraday'

class GatewayController < ApplicationController
  skip_before_action :verify_authenticity_token

  def new

  end

  def index
    puts params[:first_param]

    variables = params[:first_param].split("_")

    @name = ""
    @account = ""
    @amount = ""
    @reference = ""
    @phone = ""

    if !params[:view_campaign].nil?
      redirect_to "https://digitalbanking.stewardbank.co.zw/kanzatu/263771222727?view_campaign=f47190dc-3f33-47ba-8172-f881f3774b69"
    elsif !variables.nil?
      puts variables[0]

      @name = variables[0]
      @account = variables[1]
      @amount = variables[2]
      @reference = variables[3]
      @phone = variables[4]
      @the_amount = ((@amount.to_f)/100).to_s
    
      puts @amount
      puts @name
      puts @reference
      puts @phone
      puts @participantReference

      #poll_transaction(@amount, @name, @reference, @account)
      begin
        initialResponse = @@conn.post do |req|
          req.url 'paynow/secured/paynow/checktransaction'
          req.headers['Content-Type'] = 'application/json'
          req.body = '{ "channel": "MobileWorld", "amount": "'+@the_amount+'", "biller": "'+@name+'", "transactionId": "'+@reference+'" }'
          req.body.to_str 
          puts "here is the req body"
          puts req.body
        end
        
        puts "here is the response body" 
        puts initialResponse.body 

        bodyString = JSON.parse(initialResponse.body)

        payment = Payment.new
        payment.bill_amount = @amount
        payment.account_number = @account
        payment.reference = @token
        payment.biller_name = @name
        payment.transaction_id = @reference
          
        case bodyString["message"]
        when "FAILED"
          puts "First I am failed"
          payment.status = "FAILED"
          #payment.save!
          render 'error_page'
        when "SUCCESS"
          @token = bodyString["responseBody"]
          puts "The token is "+@token
          # payment.reference = @token
          # payment.status = "SUCCESS"
          #payment.save!
          deposit(@amount, @phone, @token, @account)
          
          render 'success'
        else
          payment.status = bodyString["message"]
          #payment.save!

          render 'error_page'
        end
        
      rescue Exception => e
        puts e.message
      end
    end
  end

  def deposit(amount, name, reference, account)
    puts "The call was made"
    puts @amount
    puts @name
    puts @reference

    @token = "";
    @amount = ((@amount.to_f)/100).to_s
    #@the_amount = ((@amount.to_f)/100).to_s

    @image_url = "http://storage.googleapis.com/squarev2-87787.appspot.com/kanzatu/#{account}.jpg"
    @campaign_address = "https://digitalbanking.stewardbank.co.zw/kanzatu/#{account}.jpg"
    @campaign_description = "Coming together is the beginning. Keeping together is progress. Working together is success. Lets all join together to make this campaign a success."
    @campaign_name = "Kanzatu-nzatu Crowdfunding"
    
    puts "Calling Poll"
    begin
      depositResponse = @@conn.post do |req|
        req.url 'gofundme/secured/campaign/deposit'
        req.headers['Content-Type'] = 'application/json'
        req.body = '{ "token": "'+reference+'", "phone": "'+name+'", "biller": "KANZATU", "amount": '+@amount+', "payerName": "'+name+'", "billAccount": "'+account+'" }'
        req.body.to_str 
        puts "here is the req body"
        puts req.body
      end
      depositString = JSON.parse(depositResponse.body)
      puts depositString["message"]
      case depositString["message"]
      when "FAILED"
        puts "First I am failed"
        render 'error_page'
      when "SUCCESS"
        render 'success'
      else
        fail "Invalid response #{response.to_str} received."
      end
    rescue Exception => e
      puts e.message
      render 'error_page'
    end
  end

  def create
  end
end
