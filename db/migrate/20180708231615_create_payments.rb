class CreatePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :payments do |t|
      t.string :transaction_id
      t.string :status
      t.string :biller_name
      t.string :bill_amount
      t.string :account_number
      t.string :reference

      t.timestamps
    end
  end
end
